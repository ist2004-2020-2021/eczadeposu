QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    Veri/VeriSiniflari/eczalisfaturasi.cpp \
    Veri/VeriSiniflari/eczeczanebilgileri.cpp \
    Veri/VeriSiniflari/eczilacalisbilgileri.cpp \
    Veri/VeriSiniflari/eczilacbilgileri.cpp \
    Veri/VeriSiniflari/eczilacsatiffaturasi.cpp \
    Veri/VeriSiniflari/eczsatisbilgileri.cpp \
    Veri/VeriSiniflari/ecztedarikci.cpp \
    Veri/VeriSiniflari/eczuretilenilac.cpp \
    main.cpp \
    eczanapencere.cpp

HEADERS += \
    Veri/VeriSiniflari/eczalisfaturasi.h \
    Veri/VeriSiniflari/eczeczanebilgileri.h \
    Veri/VeriSiniflari/eczilacalisbilgileri.h \
    Veri/VeriSiniflari/eczilacbilgileri.h \
    Veri/VeriSiniflari/eczilacsatiffaturasi.h \
    Veri/VeriSiniflari/eczsatisbilgileri.h \
    Veri/VeriSiniflari/ecztedarikci.h \
    Veri/VeriSiniflari/eczuretilenilac.h \
    eczanapencere.h

FORMS += \
    eczanapencere.ui

TRANSLATIONS += \
    EczeDeposu_tr_TR.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
