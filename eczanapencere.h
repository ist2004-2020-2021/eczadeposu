#ifndef ECZANAPENCERE_H
#define ECZANAPENCERE_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class ECZAnaPencere; }
QT_END_NAMESPACE

class ECZAnaPencere : public QMainWindow
{
    Q_OBJECT

public:
    ECZAnaPencere(QWidget *parent = nullptr);
    ~ECZAnaPencere();

private:
    Ui::ECZAnaPencere *ui;
};
#endif // ECZANAPENCERE_H
